package com.mcecelja.soundboard.enums

enum class AnimalSoundEnum {
    CAT,
    DOG,
    BIRD
}