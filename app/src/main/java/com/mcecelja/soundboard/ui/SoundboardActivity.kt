package com.mcecelja.soundboard.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mcecelja.soundboard.R
import com.mcecelja.soundboard.databinding.ActivitySoundboardBinding
import com.mcecelja.soundboard.enums.AnimalSoundEnum
import org.koin.androidx.viewmodel.ext.android.viewModel

class SoundboardActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySoundboardBinding

    private val viewModel by viewModel<SoundboardViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySoundboardBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ibCat.setOnClickListener { viewModel.playSound(AnimalSoundEnum.CAT) }
        binding.ibDog.setOnClickListener { viewModel.playSound(AnimalSoundEnum.DOG) }
        binding.ibBird.setOnClickListener { viewModel.playSound(AnimalSoundEnum.BIRD) }
    }
}