package com.mcecelja.soundboard.ui

import androidx.lifecycle.ViewModel
import com.mcecelja.soundboard.enums.AnimalSoundEnum
import com.mcecelja.soundboard.sound.AudioPlayer

class SoundboardViewModel(private val audioPlayer: AudioPlayer) : ViewModel() {

    fun playSound(sound: AnimalSoundEnum) = audioPlayer.playSound(sound)
}