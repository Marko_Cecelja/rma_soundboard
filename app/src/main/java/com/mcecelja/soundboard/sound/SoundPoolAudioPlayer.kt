package com.mcecelja.soundboard.sound

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import com.mcecelja.soundboard.R
import com.mcecelja.soundboard.enums.AnimalSoundEnum

class SoundPoolAudioPlayer(context: Context) : AudioPlayer {

    private val sounds: Map<AnimalSoundEnum, Int>

    private val soundPool: SoundPool

    init {
        soundPool =
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()

                SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .setMaxStreams(6)
                    .build()
            } else {
                SoundPool(6, AudioManager.STREAM_MUSIC, 1)
            }

        sounds = mapOf(
            AnimalSoundEnum.CAT to soundPool.load(context, R.raw.cat, 1),
            AnimalSoundEnum.DOG to soundPool.load(context, R.raw.dog, 1),
            AnimalSoundEnum.BIRD to soundPool.load(context, R.raw.bird, 1)
        )
    }

    override fun playSound(sound: AnimalSoundEnum) {
        soundPool.play(sounds[sound] ?: error("Unknown sound"), 1f, 1f, 1, 0, 1f)
    }
}