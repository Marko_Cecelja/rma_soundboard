package com.mcecelja.soundboard.sound

import com.mcecelja.soundboard.enums.AnimalSoundEnum

interface AudioPlayer {

    fun playSound(sound: AnimalSoundEnum)
}