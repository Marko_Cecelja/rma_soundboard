package com.mcecelja.soundboard.di

import com.mcecelja.soundboard.sound.AudioPlayer
import com.mcecelja.soundboard.sound.SoundPoolAudioPlayer
import com.mcecelja.soundboard.ui.SoundboardViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single<AudioPlayer> { SoundPoolAudioPlayer(androidContext()) }
}

val viewModelModule = module {
    viewModel { SoundboardViewModel(get()) }
}