package com.mcecelja.soundboard

import android.app.Application
import com.mcecelja.soundboard.di.appModule
import com.mcecelja.soundboard.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class Soundboard : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@Soundboard)
            modules(appModule, viewModelModule)
        }
    }
}